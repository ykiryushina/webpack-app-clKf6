/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./js/burgerMenu.js":
/*!**************************!*\
  !*** ./js/burgerMenu.js ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _css_styles_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../css/styles.css */ \"./css/styles.css\");\n/* harmony import */ var _img_Ellipse1_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../img/Ellipse1.png */ \"./img/Ellipse1.png\");\n/* harmony import */ var _img_fb_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../img/fb.svg */ \"./img/fb.svg\");\n/* harmony import */ var _img_insta_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../img/insta.svg */ \"./img/insta.svg\");\n/* harmony import */ var _img_Linkedin_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../img/Linkedin.svg */ \"./img/Linkedin.svg\");\n/* harmony import */ var _img_twitter_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../img/twitter.svg */ \"./img/twitter.svg\");\n/* harmony import */ var _img_Rectangle1_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../img/Rectangle1.png */ \"./img/Rectangle1.png\");\n/* harmony import */ var _img_Rectangle2_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../img/Rectangle2.png */ \"./img/Rectangle2.png\");\n/* harmony import */ var _img_Rectangle3_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../img/Rectangle3.png */ \"./img/Rectangle3.png\");\n\n\n\n\n\n\n\n\n\n\ndocument.querySelector('.menu__icon').onclick = function (event) {\n  if (event.target.className === 'menu__icon' || event.target.className === 'menu__body._active') {\n    var target = event.target;\n    target.classList.toggle('_active');\n    document.querySelector('.menu__body').classList.toggle('_active');\n  } else if (event.target.tagName == 'SPAN') {\n    event.target.parentElement.classList.toggle('_active');\n    document.querySelector('.menu__body').classList.toggle('_active');\n  }\n};\n\n//# sourceURL=webpack:///./js/burgerMenu.js?");

/***/ }),

/***/ "./img/Ellipse1.png":
/*!**************************!*\
  !*** ./img/Ellipse1.png ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/Ellipse1.png\");\n\n//# sourceURL=webpack:///./img/Ellipse1.png?");

/***/ }),

/***/ "./img/Linkedin.svg":
/*!**************************!*\
  !*** ./img/Linkedin.svg ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/Linkedin.svg\");\n\n//# sourceURL=webpack:///./img/Linkedin.svg?");

/***/ }),

/***/ "./img/Rectangle1.png":
/*!****************************!*\
  !*** ./img/Rectangle1.png ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/Rectangle1.png\");\n\n//# sourceURL=webpack:///./img/Rectangle1.png?");

/***/ }),

/***/ "./img/Rectangle2.png":
/*!****************************!*\
  !*** ./img/Rectangle2.png ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/Rectangle2.png\");\n\n//# sourceURL=webpack:///./img/Rectangle2.png?");

/***/ }),

/***/ "./img/Rectangle3.png":
/*!****************************!*\
  !*** ./img/Rectangle3.png ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/Rectangle3.png\");\n\n//# sourceURL=webpack:///./img/Rectangle3.png?");

/***/ }),

/***/ "./img/fb.svg":
/*!********************!*\
  !*** ./img/fb.svg ***!
  \********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/fb.svg\");\n\n//# sourceURL=webpack:///./img/fb.svg?");

/***/ }),

/***/ "./img/insta.svg":
/*!***********************!*\
  !*** ./img/insta.svg ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/insta.svg\");\n\n//# sourceURL=webpack:///./img/insta.svg?");

/***/ }),

/***/ "./img/twitter.svg":
/*!*************************!*\
  !*** ./img/twitter.svg ***!
  \*************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"img/twitter.svg\");\n\n//# sourceURL=webpack:///./img/twitter.svg?");

/***/ }),

/***/ "./css/styles.css":
/*!************************!*\
  !*** ./css/styles.css ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack:///./css/styles.css?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./js/burgerMenu.js");
/******/ 	
/******/ })()
;