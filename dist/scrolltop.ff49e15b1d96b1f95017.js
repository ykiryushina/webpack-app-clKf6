/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/scrollTop.js":
/*!*************************!*\
  !*** ./js/scrollTop.js ***!
  \*************************/
/***/ (function() {

eval("var entryBlockHeight = document.querySelector('.introduction').offsetHeight;\nvar scrollBtn = document.querySelector('.main__scroll-top-btn');\n\nwindow.onscroll = function () {\n  if (window.scrollY > entryBlockHeight) {\n    scrollBtn.classList.add('_active');\n  } else if (window.scrollY < entryBlockHeight) {\n    scrollBtn.classList.remove('_active');\n  }\n};\n\nfunction scrollToTop() {\n  scrollBtn.addEventListener('click', function () {\n    window.scrollTo(0, 0);\n  });\n}\n\nscrollToTop();\n\n//# sourceURL=webpack:///./js/scrollTop.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./js/scrollTop.js"]();
/******/ 	
/******/ })()
;