/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/slider.js":
/*!**********************!*\
  !*** ./js/slider.js ***!
  \**********************/
/***/ (function() {

eval("var sliderLine = document.querySelector('.works__items');\nvar allWorks = document.querySelectorAll('.works__row');\nvar elemWidth = document.querySelector('.works__row').offsetWidth;\nvar worksWidth = elemWidth * allWorks.length;\nvar count = 0;\nvar sliderWidth;\n\nfunction init() {\n  sliderWidth = document.querySelector('.slider').offsetWidth;\n  sliderLine.style.width = sliderWidth * allWorks.length + 'px';\n  allWorks.forEach(function (item) {\n    item.style.width = sliderWidth + 'px';\n    item.style.height = 'auto';\n  });\n  rollSlider();\n}\n\ninit();\nwindow.addEventListener('resize', init);\ndocument.querySelector('.slider__btn_prev').addEventListener('click', function () {\n  count--;\n\n  if (count < 0) {\n    count = allWorks.length - 1;\n  }\n\n  rollSlider();\n});\ndocument.querySelector('.slider__btn_next').addEventListener('click', function () {\n  count++;\n\n  if (count >= allWorks.length) {\n    count = 0;\n  }\n\n  rollSlider();\n});\n\nfunction rollSlider() {\n  sliderLine.style.transform = 'translate(-' + count * sliderWidth + 'px)';\n}\n\n//# sourceURL=webpack:///./js/slider.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./js/slider.js"]();
/******/ 	
/******/ })()
;