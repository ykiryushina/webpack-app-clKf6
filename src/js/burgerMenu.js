import '../css/styles.css';
import Ellipse1 from '../img/Ellipse1.png';
import fb from '../img/fb.svg';
import insta from '../img/insta.svg';
import Linkedin from '../img/Linkedin.svg';
import twitter from '../img/twitter.svg';
import Rectangle1 from '../img/Rectangle1.png';
import Rectangle2 from '../img/Rectangle2.png';
import Rectangle3 from '../img/Rectangle3.png';


document.querySelector('.menu__icon').onclick = (event) => {
    if (event.target.className === 'menu__icon' || event.target.className === 'menu__body._active') {
        const target = event.target;
        target.classList.toggle('_active');
        document.querySelector('.menu__body').classList.toggle('_active');
    } else if (event.target.tagName == 'SPAN') {
        event.target.parentElement.classList.toggle('_active');
        document.querySelector('.menu__body').classList.toggle('_active');
    }
}